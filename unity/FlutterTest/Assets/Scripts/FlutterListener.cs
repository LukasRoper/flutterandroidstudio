using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlutterListener : MonoBehaviour
{
    public Text text;

    // Add functionality here for the label to be updated using data saved from the Flutter app (i.e. set in the flutter UI and Unity is then informed to update).

    private void Awake()
    {
        GetComponent<UnityMessageManager>().OnMessage += FlutterListener_OnMessage;
    }

    private void FlutterListener_OnMessage(string message)
    {
        text.text = message;
    }
}
