﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void NativeAPI::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NativeAPI_OnSceneLoaded_m3664DB25D28063B8628236EA7338D05DEB78694A (void);
// 0x0000000A System.Void NativeAPI::SendMessageToFlutter(System.String)
extern void NativeAPI_SendMessageToFlutter_m589A00F5AE6DABEECC576A5E6BC709850513AE9B (void);
// 0x0000000B System.Void NativeAPI::ShowHostMainWindow()
extern void NativeAPI_ShowHostMainWindow_mF11F3AE0A8A16104D2546D5225A967CAEF20F494 (void);
// 0x0000000C System.Void NativeAPI::UnloadMainWindow()
extern void NativeAPI_UnloadMainWindow_mC078E094545CA94E5BAF399E7C8B75E39274A855 (void);
// 0x0000000D System.Void NativeAPI::QuitUnityWindow()
extern void NativeAPI_QuitUnityWindow_m53C753446251C8F1D17D43FAC31BFC8DAEB12607 (void);
// 0x0000000E System.Void NativeAPI::.ctor()
extern void NativeAPI__ctor_m078F288AC03B02E77E2017CB3EBD3E42B9FED0E4 (void);
// 0x0000000F T SingletonMonoBehaviour`1::get_Instance()
// 0x00000010 T SingletonMonoBehaviour`1::CreateSingleton()
// 0x00000011 System.Void SingletonMonoBehaviour`1::.ctor()
// 0x00000012 System.Void SingletonMonoBehaviour`1::.cctor()
// 0x00000013 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mFD7001A5166EDD8386B2979B3C0FDC4544D0F123 (void);
// 0x00000014 T MessageHandler::getData()
// 0x00000015 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mA68FADA526667CCEA365016E6A8503CFC5E31A32 (void);
// 0x00000016 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m9EF609A41518571430690AFA490250C054CA5C4B (void);
// 0x00000017 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m31453D518E0E54753A5581B23A7ABEFD6165C537 (void);
// 0x00000018 System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m044493EEA44FD0FE6040EAAC4F26AF0AA65D32B1 (void);
// 0x00000019 System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m7F72EFB7A6934E92EE91211BE100AAFAC60F1EF8 (void);
// 0x0000001A System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_mFBE226643D6E84B5D7931C507A60D9C965B8E2B0 (void);
// 0x0000001B System.Void UnityMessageManager::add_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_m739FF9E4E00FCB9BC32CB2D7892444941B0601AC (void);
// 0x0000001C System.Void UnityMessageManager::remove_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_m6407A819093B1AFEF9270B4453A3E479124326A5 (void);
// 0x0000001D System.Void UnityMessageManager::Start()
extern void UnityMessageManager_Start_m366A7C3E50AE07BA45A2A49FE9382E9AA838F54B (void);
// 0x0000001E System.Void UnityMessageManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void UnityMessageManager_OnSceneLoaded_mD7DA9C76ADA8D8F30198B4D274FE391150DF7192 (void);
// 0x0000001F System.Void UnityMessageManager::ShowHostMainWindow()
extern void UnityMessageManager_ShowHostMainWindow_m247203DD3FC1D1995A57C0302A6E23E594A927D2 (void);
// 0x00000020 System.Void UnityMessageManager::UnloadMainWindow()
extern void UnityMessageManager_UnloadMainWindow_m37214FA2AE636F567825DB030434D31C80223593 (void);
// 0x00000021 System.Void UnityMessageManager::QuitUnityWindow()
extern void UnityMessageManager_QuitUnityWindow_m08F6EEDA9F59BB793055A6101DD1ACD62F18F49F (void);
// 0x00000022 System.Void UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_mABF45FAA298107DDF065EC21C6925F3C7A4C3FA7 (void);
// 0x00000023 System.Void UnityMessageManager::SendMessageToFlutter(UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m72DFFAE91711298BB40833DC9C92B0D3BD778C65 (void);
// 0x00000024 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m4D0F43F8D328F97496576D9930AAB3686E6A8558 (void);
// 0x00000025 System.Void UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m1A5DC207EFB4A24E9571BDA6EBD3D8E3AFBB576F (void);
// 0x00000026 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_m03CBB4E08C8BA079E07D430AD377D38400DB82BF (void);
// 0x00000027 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_m89611F0C2BAAF282BE647049B11F835262EFDB73 (void);
// 0x00000028 System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_mABBBB0720CEC9ADB6560700A2EE640194D040EF8 (void);
// 0x00000029 System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m38A45D4D87F60621AB4E4B6A68DDDE146B332DDD (void);
// 0x0000002A System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_m5FD705274B59AF179AE22FF30A248A4A24B4BECA (void);
// 0x0000002B System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m8EB1088C4795F088A729524E99334E63ED48E6B7 (void);
// 0x0000002C System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m27BFC33CA6935FF169ED735C704019CD4DF0E8C1 (void);
// 0x0000002D System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_m001CCB602D2B55DD220BF06E2AA4828575197FFE (void);
// 0x0000002E System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m6F3C7C037B912D8BD1CB2FAB6CF0E8D8CB3825BA (void);
// 0x0000002F System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE67B6A56FC2D25FC6BAE72DF3DDA9A01260DF3D2 (void);
// 0x00000030 System.Void FlutterListener::Awake()
extern void FlutterListener_Awake_m8579F0BB14AF53C1A7075234ED45A0A76F05148B (void);
// 0x00000031 System.Void FlutterListener::FlutterListener_OnMessage(System.String)
extern void FlutterListener_FlutterListener_OnMessage_m0ADD1090A8658FB8100D1951B2112B9022AD48B6 (void);
// 0x00000032 System.Void FlutterListener::.ctor()
extern void FlutterListener__ctor_mB0276004B6720C7A4C2EB3F7B5EDEBF12100024F (void);
// 0x00000033 System.Void SetDataUnity::SetText(System.String)
extern void SetDataUnity_SetText_m12EAEB94AE4BB5FA733C6B130CC6709DBFCDEC40 (void);
// 0x00000034 System.Void SetDataUnity::.ctor()
extern void SetDataUnity__ctor_mBA81A4D815E8BDDD17941C6829F0AED7D3223136 (void);
static Il2CppMethodPointer s_methodPointers[52] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeAPI_OnSceneLoaded_m3664DB25D28063B8628236EA7338D05DEB78694A,
	NativeAPI_SendMessageToFlutter_m589A00F5AE6DABEECC576A5E6BC709850513AE9B,
	NativeAPI_ShowHostMainWindow_mF11F3AE0A8A16104D2546D5225A967CAEF20F494,
	NativeAPI_UnloadMainWindow_mC078E094545CA94E5BAF399E7C8B75E39274A855,
	NativeAPI_QuitUnityWindow_m53C753446251C8F1D17D43FAC31BFC8DAEB12607,
	NativeAPI__ctor_m078F288AC03B02E77E2017CB3EBD3E42B9FED0E4,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageHandler_Deserialize_mFD7001A5166EDD8386B2979B3C0FDC4544D0F123,
	NULL,
	MessageHandler__ctor_mA68FADA526667CCEA365016E6A8503CFC5E31A32,
	MessageHandler_send_m9EF609A41518571430690AFA490250C054CA5C4B,
	UnityMessage__ctor_m31453D518E0E54753A5581B23A7ABEFD6165C537,
	UnityMessageManager_generateId_m044493EEA44FD0FE6040EAAC4F26AF0AA65D32B1,
	UnityMessageManager_add_OnMessage_m7F72EFB7A6934E92EE91211BE100AAFAC60F1EF8,
	UnityMessageManager_remove_OnMessage_mFBE226643D6E84B5D7931C507A60D9C965B8E2B0,
	UnityMessageManager_add_OnFlutterMessage_m739FF9E4E00FCB9BC32CB2D7892444941B0601AC,
	UnityMessageManager_remove_OnFlutterMessage_m6407A819093B1AFEF9270B4453A3E479124326A5,
	UnityMessageManager_Start_m366A7C3E50AE07BA45A2A49FE9382E9AA838F54B,
	UnityMessageManager_OnSceneLoaded_mD7DA9C76ADA8D8F30198B4D274FE391150DF7192,
	UnityMessageManager_ShowHostMainWindow_m247203DD3FC1D1995A57C0302A6E23E594A927D2,
	UnityMessageManager_UnloadMainWindow_m37214FA2AE636F567825DB030434D31C80223593,
	UnityMessageManager_QuitUnityWindow_m08F6EEDA9F59BB793055A6101DD1ACD62F18F49F,
	UnityMessageManager_SendMessageToFlutter_mABF45FAA298107DDF065EC21C6925F3C7A4C3FA7,
	UnityMessageManager_SendMessageToFlutter_m72DFFAE91711298BB40833DC9C92B0D3BD778C65,
	UnityMessageManager_onMessage_m4D0F43F8D328F97496576D9930AAB3686E6A8558,
	UnityMessageManager_onFlutterMessage_m1A5DC207EFB4A24E9571BDA6EBD3D8E3AFBB576F,
	UnityMessageManager__ctor_m03CBB4E08C8BA079E07D430AD377D38400DB82BF,
	UnityMessageManager__cctor_m89611F0C2BAAF282BE647049B11F835262EFDB73,
	MessageDelegate__ctor_mABBBB0720CEC9ADB6560700A2EE640194D040EF8,
	MessageDelegate_Invoke_m38A45D4D87F60621AB4E4B6A68DDDE146B332DDD,
	MessageDelegate_BeginInvoke_m5FD705274B59AF179AE22FF30A248A4A24B4BECA,
	MessageDelegate_EndInvoke_m8EB1088C4795F088A729524E99334E63ED48E6B7,
	MessageHandlerDelegate__ctor_m27BFC33CA6935FF169ED735C704019CD4DF0E8C1,
	MessageHandlerDelegate_Invoke_m001CCB602D2B55DD220BF06E2AA4828575197FFE,
	MessageHandlerDelegate_BeginInvoke_m6F3C7C037B912D8BD1CB2FAB6CF0E8D8CB3825BA,
	MessageHandlerDelegate_EndInvoke_mE67B6A56FC2D25FC6BAE72DF3DDA9A01260DF3D2,
	FlutterListener_Awake_m8579F0BB14AF53C1A7075234ED45A0A76F05148B,
	FlutterListener_FlutterListener_OnMessage_m0ADD1090A8658FB8100D1951B2112B9022AD48B6,
	FlutterListener__ctor_mB0276004B6720C7A4C2EB3F7B5EDEBF12100024F,
	SetDataUnity_SetText_m12EAEB94AE4BB5FA733C6B130CC6709DBFCDEC40,
	SetDataUnity__ctor_mBA81A4D815E8BDDD17941C6829F0AED7D3223136,
};
static const int32_t s_InvokerIndices[52] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2697,
	2904,
	2948,
	2948,
	2948,
	1886,
	-1,
	-1,
	-1,
	-1,
	2842,
	-1,
	396,
	1583,
	1886,
	2931,
	1583,
	1583,
	1583,
	1583,
	1886,
	986,
	1886,
	1886,
	1886,
	1583,
	1583,
	1583,
	1583,
	1886,
	2948,
	972,
	1583,
	521,
	1583,
	972,
	1583,
	521,
	1583,
	1886,
	1583,
	1886,
	1583,
	1886,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x02000004, { 21, 9 } },
	{ 0x06000014, { 30, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[31] = 
{
	{ (Il2CppRGCTXDataType)2, 684 },
	{ (Il2CppRGCTXDataType)3, 4053 },
	{ (Il2CppRGCTXDataType)2, 1081 },
	{ (Il2CppRGCTXDataType)3, 4051 },
	{ (Il2CppRGCTXDataType)3, 4102 },
	{ (Il2CppRGCTXDataType)2, 1103 },
	{ (Il2CppRGCTXDataType)3, 4100 },
	{ (Il2CppRGCTXDataType)3, 4118 },
	{ (Il2CppRGCTXDataType)2, 1111 },
	{ (Il2CppRGCTXDataType)3, 4116 },
	{ (Il2CppRGCTXDataType)3, 4127 },
	{ (Il2CppRGCTXDataType)2, 1115 },
	{ (Il2CppRGCTXDataType)3, 4125 },
	{ (Il2CppRGCTXDataType)3, 4052 },
	{ (Il2CppRGCTXDataType)3, 4101 },
	{ (Il2CppRGCTXDataType)3, 4117 },
	{ (Il2CppRGCTXDataType)3, 4126 },
	{ (Il2CppRGCTXDataType)2, 254 },
	{ (Il2CppRGCTXDataType)2, 513 },
	{ (Il2CppRGCTXDataType)2, 598 },
	{ (Il2CppRGCTXDataType)2, 623 },
	{ (Il2CppRGCTXDataType)2, 2398 },
	{ (Il2CppRGCTXDataType)3, 6238 },
	{ (Il2CppRGCTXDataType)1, 355 },
	{ (Il2CppRGCTXDataType)3, 12429 },
	{ (Il2CppRGCTXDataType)3, 9480 },
	{ (Il2CppRGCTXDataType)2, 1182 },
	{ (Il2CppRGCTXDataType)3, 4520 },
	{ (Il2CppRGCTXDataType)2, 1992 },
	{ (Il2CppRGCTXDataType)3, 6237 },
	{ (Il2CppRGCTXDataType)3, 12419 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	52,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	31,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
